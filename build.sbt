import com.typesafe.sbt.SbtNativePackager.autoImport._
import sbt.Keys._
import sbt.Project.projectToRef
import PlayKeys._

// The Typesafe repository
//resolvers ++= Seq(
//  Resolver.sonatypeRepo("releases"),
//  Resolver.bintrayRepo("insign", "play-cms")
//)

scalaVersion := Settings.versions.scala


lazy val server = Settings.playcms(Project("server", file("server")))
  .settings(
    name := Settings.name,
    organization := Settings.organization,
    version := Settings.version,
    scalaVersion := Settings.versions.scala,
    scalacOptions ++= Settings.scalacOptions,
    libraryDependencies ++= Settings.jvmDependencies.value,
    libraryDependencies ++= Settings.playcms.libraryDependencies,
    // connect to the client project
    scalaJSProjects := clients, // remove this if you don't use scala-js
    pipelineStages := Seq(scalaJSProd, uglify, digest, gzip), // remove scalaJSProd if you don't use scala-js

    resolvers ++= Settings.resolvers.value,

    // don't fork during tests
    Keys.fork in Test := false,
    // a simple test support of non-dockerized application(for the usage of sbt:test outside of docker)
    javaOptions in Test += "-Dconfig.file=conf/application.test.conf",

    routesGenerator := InjectedRoutesGenerator,
    devSettings += ("play.http.router", "metronic.Routes"),
    devSettings += ("play.http.router", "commons.Routes"),
    devSettings += ("play.http.router", "cms.Routes"),

    // fixes problem with loading entities in prod mode,
    // see: https://github.com/playframework/playframework/issues/4590
    PlayKeys.externalizeResources := false,


    // Root Path for included CSS in main.less
    LessKeys.rootpath := "/",
    // Adjust Urls imported in main.less
    LessKeys.relativeUrls := true,
    // compress CSS
    LessKeys.compress in Assets := true
  )
  .enablePlugins(PlayJava)
  .aggregate(clients.map(projectToRef): _*) // remove this if you don't use scala-js
  .dependsOn(sharedJVM) // remove this if you don't use scala-js

// Native packaging options


// Enable .deb builds
// See: http://www.scala-sbt.org/sbt-native-packager/formats/debian.html
// TODO: Play needs custom treatment for pid file and application.conf: http://www.scala-sbt.org/sbt-native-packager/topics/play.html
//enablePlugins(DebianPlugin)
//
//name := "play-cms-demo"
//
//version := "1.0-SNAPSHOT"
//
//maintainer := "Martin Bachmann <m.bachmann@insign.ch>"
//
//packageSummary := "The play-cms demo application."
//
//packageDescription := A package description of our software,
//  with multiple lines."""
//
//
//// Settings for pid file and application.conf
//// See: http://www.scala-sbt.org/sbt-native-packager/topics/play.html
//javaOptions in Universal ++= Seq(
//  // JVM memory tuning
//  "-J-Xmx1024m",
//  "-J-Xms512m",
//
//  // Since play uses separate pidfile we have to provide it with a proper path
//  s"-Dpidfile.path=/var/run/${packageName.value}/play.pid",
//
//  // Use separate configuration file for production environment
//  s"-Dconfig.file=/usr/share/${packageName.value}/conf/production.conf",
//
//  // Use separate logger configuration file for production environment
//  s"-Dlogger.file=/usr/share/${packageName.value}/conf/production-logger.xml"
//)


// Enable docker builds
// Build using: sbt docker:publishLocal
// See: http://www.scala-sbt.org/sbt-native-packager/formats/docker.html or https://github.com/muuki88/sbt-native-packager-examples/tree/master/play-2.3


// If you want to develop the play-* modules, you need to make next steps:
//  - Clone play-cms
//    git clone git@bitbucket.org:insigngmbh/play-cms.git play-cms
//  - Call sbt with sys prop "-Dplaycms.devmode=true" or put .sbopts file
//    into the project root with with the line: "-J-Dplaycms.devmode=true"


//////////////////////////
// ScalaJs Integration
//////////////////////////

// This demo-project uses Scala-js in the frontend. Scala-js Projects are usually grouped into three individual projects
// shared, client and server (the actual play-application).
// if you want to write frontend-script in Javascript, you can remove all code in this Section, but you have to make sure, that you also get rid of the following things:
// - remove scala-js-related project-folders: shared and client. you are left with server, which is your actual play-appliation
// - remove scala-js plugin from plugins.sbt
// - remove scala-js relevant settings from server-project (above)
// - get rid of scala-js includes in main.scala.html template and use conventional javascript-includes instead
// - remove sample-implementation of Autowire-API as provided in Application, at the following two places: Application.scala::autowireApi and ApiService.scala
// - remove dependencies for unused project in project/Settings.scala


// crossProject for configuring a JS/JVM/shared structure
lazy val shared = (crossProject.crossType(CrossType.Pure) in file("shared"))
  .settings(
    scalaVersion := Settings.versions.scala,
    libraryDependencies ++= Settings.sharedDependencies.value,
    resolvers ++= Settings.resolvers.value
  )
  // set up settings specific to the JS project
  .jsConfigure(_ enablePlugins ScalaJSPlay)

lazy val sharedJVM = shared.jvm.settings(name := "sharedJVM")

lazy val sharedJS = shared.js.settings(name := "sharedJS")

// instantiate the scala-js project
lazy val client: Project = (project in file("client"))
  .settings(
    name := "client",
    version := Settings.version,
    scalaVersion := Settings.versions.scala,
    scalacOptions ++= Settings.scalacOptions,
    libraryDependencies ++= Settings.scalajsDependencies.value,
    // by default we do development build, no eliding
    jsDependencies ++= Settings.jsDependencies.value,
    // RuntimeDOM is needed for tests
    jsDependencies += RuntimeDOM % "test",
    // yes, we want to package JS dependencies
    skip in packageJSDependencies := false,
    // use Scala.js provided launcher code to start the client app
    persistLauncher := true,
    persistLauncher in Compile := true,
    persistLauncher in Test := false,
    // use uTest framework for tests
    testFrameworks += new TestFramework("utest.runner.Framework"),
    resolvers ++= Settings.resolvers.value
    // Scala-Js Workbench (Live-Reload and such things)
    //    workbenchSettings,
    //    updateBrowsers <<= updateBrowsers.triggeredBy(fastOptJS in Compile),
    //    bootSnippet := "App().bootstrap();",
    //    localUrl := ("127.0.0.1", 12345)

  )
  .enablePlugins(ScalaJSPlugin, ScalaJSPlay)
  .dependsOn(sharedJS)

// Client projects (just one in this case)
lazy val clients = Seq(client)


//////////////////////////
// End of ScalaJs Integration
//////////////////////////





//scalaJSStage in Global := FastOptStage
//
//
//persistLauncher in Compile := false
//
//persistLauncher in Test := false
//

enablePlugins(DockerPlugin)
maintainer in Docker := "Martin Bachmann <m.bachmann@insign.ch>"
packageSummary in Docker := "The play-cms demo application."
aggregate in Docker := false
dockerRepository := Some("insign-docker-registry.bintray.io")


// Change max-filename-length for scala-Compiler. Longer filenames (not sure what the threshold is) causes problems
// with encrypted home directories under ubuntu
//scalacOptions ++= Seq("-Xmax-classfile-name", "100")



// Resolve only newly added dependencies
updateOptions := updateOptions.value.withCachedResolution(true)

// loads the Play server project at sbt startup
onLoad in Global := (Command.process("project server", _: State)) compose (onLoad in Global).value