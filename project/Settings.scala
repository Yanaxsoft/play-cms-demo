import sbt._
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import play.sbt.PlayImport._

/**
  * Application settings. Configure the build for your application here.
  * You normally don't have to touch the actual build definition after this.
  */
object Settings {
  /** The application name */
  val name = """play-cms-demo"""

  /** The application version */
  val version = """1.1"""

  /** Creating organization **/
  val organization = """insign.ch"""

  /** Options for the scala compiler */
  val scalacOptions = Seq(
    "-Xlint",
    "-unchecked",
    "-deprecation",
    "-feature"
  )

  /** Declare global dependency versions here to avoid mismatches in multi part dependencies */
  object versions {
    val scala = "2.11.7"

    // CMS version
    val playCms = "2.1.66"

    // vendor libraries
    val scalajsDom = "0.9.0"
    val scalajsTools = "0.6.6"
    val autowire = "0.2.5"
    val booPickle = "1.1.2"
    val uTest = "0.3.1"
    val jQueryFacade = "1.0-RC3"
    val log4js = "1.4.10"
    val flexSlider = "2.2.2"
    val jqueryForm = "3.51"
    val jqueryUI = "1.10.3"
    val jQuery = "1.10.2"
    val bootstrap = "3.3.2"
    val playScripts = "0.4.0"
    val fontAwesome = "3.2.1"
  }

  /**
    * These dependencies are shared between JS and JVM projects
    * the special %%% function selects the correct version for each project
    */
  val sharedDependencies = Def.setting(Seq(
    "com.lihaoyi" %%% "autowire" % versions.autowire,
    "me.chrons" %%% "boopickle" % versions.booPickle
  ))

  /** Dependencies only used by the JVM project */
  val jvmDependencies = Def.setting(Seq(
    javaCore,
    javaJdbc,
    javaJpa,
    cache,
    "com.vmunier" %% "play-scalajs-scripts" % versions.playScripts,
    "org.webjars" % "font-awesome" % versions.fontAwesome,
    "org.webjars.bower" % "bootstrap" % versions.bootstrap,
    "org.webjars" % "FlexSlider" % versions.flexSlider,
    "org.webjars" % "jquery-ui" % versions.jqueryUI % Provided,
    "mysql" % "mysql-connector-java" % "5.1.38",
    "org.eclipse.persistence" % "eclipselink" % "2.6.0",
    "com.google.inject" % "guice" % "4.0",
    "javax.inject" % "javax.inject" % "1",
    "commons-lang" % "commons-lang" % "2.6",
    "com.typesafe.akka" %% "akka-actor" % "2.3.11",
    "org.mockito" % "mockito-all" % "1.9.5"
  ))

  /** CMS Dependencies used by the JVM project */
  val playcms = Modules(local = Option(sys.props("playcms.devmode")).map(_.toLowerCase).exists(_ == "true"))(
    ("ch.insign" %% "play-cms" % versions.playCms, ProjectRef(file("play-cms"), "cms")),
    ("ch.insign" %% "play-auth" % versions.playCms, ProjectRef(file("play-cms"), "auth")),
    ("ch.insign" %% "play-commons" % versions.playCms, ProjectRef(file("play-cms"), "commons")),
    ("ch.insign" %% "play-theme-metronic" % versions.playCms, ProjectRef(file("play-cms"), "metronic")))



  /** Dependencies only used by the JS project (note the use of %%% instead of %%) */
  val scalajsDependencies = Def.setting(Seq(
    "org.scala-js" %%% "scalajs-dom" % versions.scalajsDom,
    "org.scala-js" %%% "scalajs-tools" % versions.scalajsTools,
    "com.lihaoyi" %%% "utest" % versions.uTest % Test,
    "org.querki" %%% "jquery-facade" % versions.jQueryFacade
  ))

  /** Dependencies for external JS libs that are bundled into a single .js file according to dependency order */
  val jsDependencies = Def.setting(Seq(
    "org.webjars" % "jquery" % versions.jQuery / "jquery.js" minified "jquery.min.js",
    "org.webjars" % "bootstrap" % versions.bootstrap / "bootstrap.js" minified "bootstrap.min.js" dependsOn "jquery.js",
    "org.webjars" % "log4javascript" % versions.log4js / "js/log4javascript_uncompressed.js" minified "js/log4javascript.js",
    "org.webjars" % "FlexSlider" % versions.flexSlider / "jquery.flexslider.js" minified "jquery.flexslider-min.js",
    "org.webjars" % "jquery-form" % versions.jqueryForm / "jquery.form.js",
    "org.webjars" % "jquery-ui" % versions.jqueryUI / "ui/jquery-ui.js" minified "ui/minified/jquery-ui.min.js"
  ))


  val resolvers = Def.setting(Seq(
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots"),
    Resolver.bintrayRepo("insign", "play-cms")
  ))
}