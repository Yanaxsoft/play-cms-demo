import sbt.Keys._

// Comment to get more information during initialization
logLevel := Level.Warn

// The Typesafe repository
resolvers += "Typesafe repository" at "https://dl.bintray.com/typesafe/maven-releases/"

resolvers += "Sonatype Repository" at "https://oss.sonatype.org/content/groups/public"

// Dependency resolving

resolvers += "Sonatype repository" at "https://oss.sonatype.org/content/repositories/snapshots/"

resolvers += "spray repo" at "http://repo.spray.io"

resolvers += Resolver.bintrayRepo("insign", "play-cms")

// repository for Typesafe plugins

// Use the Play sbt plugin for Play projects
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.3")

// Generates getters and setters for Java properties
addSbtPlugin("com.typesafe.sbt" % "sbt-play-enhancer" % "1.1.0")

// Enable native packaging options (see http://www.scala-sbt.org/sbt-native-packager/)
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.0.3")

// Plugin for publishing to bintray
addSbtPlugin("me.lessis" % "bintray-sbt" % "0.3.0")

// Scala-js plugin to compile scala-code to javascript
addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.6")

// Integrates Scala-js into PlayFramework
addSbtPlugin("com.vmunier" % "sbt-play-scalajs" % "0.3.0")

// TODO: Workbench (forwards Scala console to browser), Life-Reload and other nice things, but doesen't work since
// TODO: sbt inside docker doesn't get inotify-events (Filesystem-events) which would trigger recompiles
// TODO: Also issues with port-mapping. workbench does not work throgh mapped docker port. )
// addSbtPlugin("com.lihaoyi" % "workbench" % "0.2.3")

addSbtPlugin("com.typesafe.sbt" % "sbt-gzip" % "1.0.0")

// Fingerprint Assets in Production
addSbtPlugin("com.typesafe.sbt" % "sbt-digest" % "1.0.0")

// Make ugly javascript even uglier
addSbtPlugin("com.typesafe.sbt" % "sbt-uglify" % "1.0.3")

// Interpret Sams work
addSbtPlugin("com.typesafe.sbt" % "sbt-less" % "1.0.6")


