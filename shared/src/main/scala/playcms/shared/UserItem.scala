package playcms.shared

case class UserItem(id: String, name: String, email: String)