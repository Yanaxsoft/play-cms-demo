package playcms.shared

trait Api {
  // message of the day
  def simpleAjaxCall(name: String): String

  // get User items
  def getUsers(): Seq[UserItem]
}
