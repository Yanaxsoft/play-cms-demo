#!/usr/bin/env bash

# A wrapper for bundled jenkins-cli.jar util

docker exec -i playcmsdemo_jenkins_1 \
    java -jar /var/jenkins_home/war/WEB-INF/jenkins-cli.jar -s http://localhost:8080 "$@"