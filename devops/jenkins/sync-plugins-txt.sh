#!/usr/bin/env bash

# Update plugins.txt file with currently installed plugins list

PLUGINS_FILE="$(dirname $0)/plugins.txt"

printf "# Run $(basename $0) to update the list of installed plugins.\n\n" \
    > $PLUGINS_FILE

docker exec playcmsdemo_jenkins_1 \
    find  /var/jenkins_home/plugins -type f -name "pom.properties" -exec mvn-artifact.sh {} \; \
    >> $PLUGINS_FILE
