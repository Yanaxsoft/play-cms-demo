import jenkins.model.*


def jobName1 = "build-job"
def configXml1 = new File('/var/jenkins_home/templates/build_config.xml').getText('UTF-8')
def xmlStream1 = new ByteArrayInputStream(configXml1.getBytes())

job1 = Jenkins.instance.getItemByFullName("build-job")
if (job1 == null) {
    Jenkins.instance.createProjectFromXML(jobName1, xmlStream1)
}

