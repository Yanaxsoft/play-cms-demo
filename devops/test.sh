#!/usr/bin/env bash

source $(dirname $0)/common.sh

WORKDIR_UID=$WORKDIR_UID \
SBT_OPTS=$SBT_OPTS \
   docker-compose -p $COMPOSE_PROJECT_NAME -f test.yml run --rm app
