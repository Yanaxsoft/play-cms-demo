#!/usr/bin/env bash

BASE_PATH=$(dirname $0)

source ${BASE_PATH}/common.sh

if [[ -n "$@" ]]; then
    CMD="$@"
else
    CMD="up -d"
fi

if [[ -z "$PROXY_PORT" ]]; then
    PROXY_PORT="9000"
fi

PROXY_PORT=$PROXY_PORT \
WORKDIR_UID=$WORKDIR_UID \
SBT_OPTS=$SBT_OPTS \
   docker-compose -p $COMPOSE_PROJECT_NAME -f ${BASE_PATH}/app.yml $CMD
