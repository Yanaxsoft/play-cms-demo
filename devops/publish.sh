#!/usr/bin/env bash

set -ex

#
# Note: This script is meant to be run by jenkins
# In order to run it locally you need to export:
# export JOB_NAME=build_play_cms_demo
# export BUILD_NUMBER=1.0-SNAPSHOT
# export JENKINS_HOME=$HOME
#

source $(dirname $0)/common.sh
#
#if [ -z "${CMS_LOG_DIR}" ]; then
#    echo "${0} - Warning: \${CMS_LOG_DIR} is not set, logs will go to /tmp/"
#    CMS_LOG_DIR="/tmp/"
#fi

RELEASES="/var/releases/$(dirname ${JOB_NAME})"

ARTIFACT_DOCKER_NAME="play-cms-website"
ARTIFACT_VERSION="$(basename ${JOB_NAME})-${BUILD_NUMBER}"
ARTIFACT_FILE="${ARTIFACT_VERSION}.tar"

ARTIFACT_VERSION=${ARTIFACT_VERSION} \
WORKDIR_UID=$WORKDIR_UID \
DOCKER_BIN=$DOCKER_BIN \
HOST_WORKDIR=$HOST_WORKDIR \
   docker-compose -p $COMPOSE_PROJECT_NAME -f publish.yml run --rm app

echo "Publishing docker container to ${RELEASES}..."

mkdir -p ${RELEASES}
docker save -o ${RELEASES}/${ARTIFACT_FILE} \
${ARTIFACT_DOCKER_NAME}:${ARTIFACT_VERSION}
#-v ${CMS_LOG_DIR}:/usr/src/app/server/logs \

cd ${RELEASES}
gzip ${ARTIFACT_FILE}

echo "Published docker container to ${RELEASES}/${FILE_GZ}..."
