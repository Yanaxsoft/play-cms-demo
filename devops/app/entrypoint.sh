#!/usr/bin/env bash

set -e

# Add sbt to host docker group
if [ -f "/var/run/docker.sock" ]; then
    DOCKER_GID=`stat -c "%g" /var/run/docker.sock`
    groupadd -fg $DOCKER_GID dockerhost
    usermod -aG $DOCKER_GID sbt
fi

# Make sure `sbt` user has the same uid as owner of the shared volume at /usr/src/app
if [ -n "$WORKDIR_UID" ]; then
    usermod -u $WORKDIR_UID sbt
    chown -R sbt:sbt /usr/src/app
    chown -R sbt:sbt /home/sbt
fi

if [ "$1" = 'sbt' ]; then
    export HOME=/home/sbt
    exec gosu sbt "$@"
else
    exec "$@"
fi