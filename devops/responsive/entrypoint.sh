#!/usr/bin/env bash

set -e

if [ -n "$WORKDIR_UID" ]; then
    usermod -u $WORKDIR_UID www-data
    chown -R www-data:www-data /var/lock/apache2 /var/run/apache2 /var/log/apache2 /var/www/html
fi

exec apache2 -DFOREGROUND "$@"