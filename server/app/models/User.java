package models;


import ch.insign.commons.i18n.Language;
import ch.insign.playauth.party.ISOGender;
import ch.insign.playauth.party.support.DefaultParty;
import play.i18n.Lang;
import play.i18n.Messages;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Demo user class extends DefaultParty with custom fields
 */
@Entity
public class User extends DefaultParty {

    @Enumerated(EnumType.STRING)
    public Gender gender;

    public String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    /**
     * Enum class for the gender of the person.
     */
    public enum Gender {
        male("male"),
        female("female");
        public final String id;

        Gender(String id) {
            this.id = id;
        }
    }
}
