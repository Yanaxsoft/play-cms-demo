package inject;

import ch.insign.playauth.authz.AuthorizationHandler;
import ch.insign.playauth.party.PartyManager;
import play.inject.guice.GuiceApplicationBuilder;
import play.inject.guice.GuiceApplicationLoader;
import security.DefaultAuthorizationHandler;
import security.DefaultPartyManager;

import static play.inject.Bindings.bind;


public class CustomApplicationLoader extends GuiceApplicationLoader {

    // Note: docker:publishLocal doesn't work without specifying play.ApplicationLoader.Context
    @Override
    public GuiceApplicationBuilder builder(play.ApplicationLoader.Context context) {
        return super.builder(context)
                .overrides(
                        // Override Guice bindings
                        bind(AuthorizationHandler.class).to(DefaultAuthorizationHandler.class),
                        bind(PartyManager.class).to(DefaultPartyManager.class));
    }
}
