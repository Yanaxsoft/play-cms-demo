package crud.controllers;

import ch.insign.cms.controllers.GlobalActionWrapper;
import ch.insign.cms.permissions.aop.RequiresBackendAccess;
import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.commons.db.Paginate;
import ch.insign.commons.db.SecureForm;
import ch.insign.commons.db.SmartForm;
import ch.insign.playauth.PlayAuth;
import com.uaihebert.model.EasyCriteria;
import crud.models.Car;
import crud.permissions.CarPermission;
import org.apache.commons.lang3.StringUtils;
import play.data.Form;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import javax.inject.Inject;

/**
 * Encapsulates a business logic tied to cars management
 */
@With({GlobalActionWrapper.class})
@RequiresBackendAccess
public class CarController extends Controller {
    private MessagesApi messagesApi;

    @Inject
    public CarController(MessagesApi messagesApi) {
        this.messagesApi = messagesApi;
    }

    /**
     * Initializes car form includes validation rules
     * @return edit template with secure form to render
     */
    @Transactional
    public Result add() {
        /**
         * Prevent unauthorized users to access this page.
         * @see {https://confluence.insign.ch/display/PLAY/Play+Auth+-+Authorization#PlayAuth-Authorization-ProtectingControllers}
         */
        PlayAuth.requirePermission(CarPermission.ADD);

        Form<Car> form = SmartForm.form(Car.class);

        /**
         * Protect the form from parameter tampering attacks.
         * @see {https://confluence.insign.ch/display/PLAY/Play+Commons#PlayCommons-SecureForm}
         */
        return ok(SecureForm.signForms(crud.views.html.edit.render(form, null)));
    }

    /**
     * Binds data from request, validate its data and if hasn't errors, persist its data to database
     * @return redirect to car list page if new car was saved or bad request, if something was wrong
     */
    @Transactional
    public Result doAdd() {
        PlayAuth.requirePermission(CarPermission.ADD);

        /**
         * Special class for our Forms: SmartForm. This class extends the normal form, but is much mightier.
         * @see {https://confluence.insign.ch/display/PLAY/Play+Commons#PlayCommons-SmartForm}
         */
        Form<Car> form = SmartForm
                .form(Car.class)
                .bindFromRequest();
        if (form.hasErrors()) {
              return badRequest(SecureForm.signForms(crud.views.html.edit.render(form, null)));
        }
        Car car = form.get();
        car.save();

        flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(), "example.crud.car.add.successful.message", car.getModel()));

        return redirect(routes.CarController.list(1, Car.PAGINATE_DEFAULT, ""));
    }

    /**
     * Finds a car by specified id and if exists, initialize a filled form includes validation rules
     * @param id - car id
     * @return edit template with secure form to render or not found page with error message
     */
    @Transactional
    public Result edit(String id) {
        Car car = Car.find.byId(id);
        if (car == null) {
            return ch.insign.cms.utils.Error.notFound(messagesApi.get(lang(), "example.crud.car.notfound.message", id));
        }
        PlayAuth.requirePermission(CarPermission.EDIT, car);

        Form<Car> form = SmartForm
                .form(Car.class)
                .fill(car);

        return ok(SecureForm.signForms(crud.views.html.edit.render(form, car)));
    }

    /**
     * Binds data from request, validate its data and saves its if hasn't errors
     * @param id - car id
     * @return redirect to car list page if success or throws bad request is errors present
     */
    @Transactional
    public Result doEdit(String id) {
        Car car = Car.find.byId(id);
        if (car == null) {
            return ch.insign.cms.utils.Error.notFound(messagesApi.get(lang(), "example.crud.car.notfound.message", id));
        }
        PlayAuth.requirePermission(CarPermission.EDIT, car);

        Form<Car> form = SmartForm
                .form(Car.class)
                .fill(car)
                .bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(SecureForm.signForms(crud.views.html.edit.render(form, car)));
        }
        form.get().save();

        flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(), "example.crud.car.add.update.message", form.get().getModel()));

        return redirect(routes.CarController.list(1, Car.PAGINATE_DEFAULT, ""));
    }

    /**
     * Delete a car by specified id
     * @param id - car id
     * @return redirect to car list page if success or not found status, if no car present with specified id
     */
    @Transactional
    public Result delete(String id) {
        Car car = Car.find.byId(id);
        if (car == null) {
            return ch.insign.cms.utils.Error.notFound(messagesApi.get(lang(), "example.crud.car.notfound.message", id));
        }
        PlayAuth.requirePermission(CarPermission.DELETE, car);

        car.delete();

        flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(), "example.crud.car.delete.successful.message"));

        return redirect(routes.CarController.list(1, Car.PAGINATE_DEFAULT, ""));
    }

    /**
     * Prints a list with all cars with possibility to filter its data by brand/pagination
     * @param page - the number of current page
     * @param itemsPerPage - per page amount
     * @param brandId - used to filter data by brand
     * @return a page - the list of cars with pagination to render
     */
    @Transactional
    public Result list(int page, int itemsPerPage, String brandId) {
        PlayAuth.requirePermission(CarPermission.BROWSE);

        EasyCriteria<Car> cars;
        if (StringUtils.isNotEmpty(brandId)) {
            cars = Car.find.findByBrand(brandId);
        } else {
            cars = Car.find.query();
        }

        Paginate<Car> pages = new Paginate<>(cars, itemsPerPage);

        return ok(crud.views.html.list.render(pages, page, brandId));
    }
}
