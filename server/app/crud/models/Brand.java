package crud.models;

import ch.insign.commons.db.Model;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "example_crud_brand")
public class Brand extends Model {
    public static BrandFinder find = new BrandFinder();

    @Constraints.Required
    @Constraints.MaxLength(value = 48)
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * BrandFinder class. Contains methods to find brands from the database.
     */
    public static class BrandFinder extends Model.Finder<Brand> {
        BrandFinder() {
            super(Brand.class);
        }
    }
}
