package crud.block;

import ch.insign.cms.models.PageBlock;
import crud.models.Car;
import play.data.Form;
import play.mvc.Controller;
import play.twirl.api.Html;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Block class that extends PageBlock archetype to represent car page and enhance its with navigation
 * {@link http://play-cms.com/doc/cms/create-your-own-cms-block}
 */
@Entity
@Table(name = "car_inventory_block")
@DiscriminatorValue("CarInventoryBlock")
public class CarInventoryBlock extends PageBlock {
    private String dealerTitle;

    @Override
    public Html render() {
        return crud.block.html.carInventoryBlock.render(this, Car.find.all());
    }

    public Html editForm(Form editForm) {
        return crud.block.html.edit.render(this, editForm, Controller.request().getQueryString("backURL"), null);
    }

    public String getDealerTitle() {
        return dealerTitle;
    }

    public void setDealerTitle(String dealerTitle) {
        this.dealerTitle = dealerTitle;
    }
}
