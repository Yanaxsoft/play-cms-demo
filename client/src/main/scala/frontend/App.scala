package frontend

import frontend.services.AjaxClient
import org.querki.jquery._
import org.scalajs.dom._
import playcms.shared.Api

import scala.scalajs.js
import scala.scalajs.js._
import scala.scalajs.js.timers._
import scala.scalajs.js.annotation._
import js.Dynamic.{global => g}
import js.Dynamic.{literal => l}
import scala.scalajs.runtime.UndefinedBehaviorError
import autowire._
import boopickle.Default._
import frontend.controllers.UserController
import logger._

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue



@JSExport("App")
object  App extends js.JSApp {
  /**
   * Main Entry-Point when not triggered by Script-Tag in Html-Head
   */
  @JSExport
  def main() = {
    timers.setInterval(100)(() => bootstrap())
    log.enableServerLogging("/logging")
    log.warn("User started main Method")

    $("document").ready(() => bootstrap() )
  }

  /**
   * starts the bootstrapping process by calling init on all frontend-related classes
   */
  @JSExport("bootstrap")
  def bootstrap(): Unit = {
    console.log("starting frontend application")
    console.log("starting frontend application")

    UserController.init

    AjaxClient[Api].simpleAjaxCall("return me this string").call().map((response) => console.log("Server responded with: " + response) )

    UserController.logUsers()
  }
}
