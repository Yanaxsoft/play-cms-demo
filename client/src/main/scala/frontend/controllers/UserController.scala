package frontend.controllers

import frontend.services.AjaxClient
import org.querki.jquery._
import org.scalajs.dom._
import playcms.shared.Api

import scala.scalajs.js
import scala.scalajs.js.Dynamic.{global => g, literal => l}
import scala.scalajs.js.annotation._
import autowire._
import boopickle.Default._
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

object  UserController extends AbstractController {
  /**
   * Initialize all User-specific functionality.
   * Event binding should happen here
   */
  def init() = {
    console.log("initializing UserController")
  }

  /**
    * logs serverside Users to console (This method only exists for educational purposes)
    * FIXME: You might not wat to have this in your code!!!!!!
    */
  def logUsers(): Unit = {
    AjaxClient[Api].getUsers().call().map(
      (users) => console.log(users.toString)
    )
  }
}
